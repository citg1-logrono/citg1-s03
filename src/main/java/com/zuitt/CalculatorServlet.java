package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 43283931101411089L;
	
	public void init() throws ServletException {
		System.out.println("******************************************");
		System.out.println(" CalculatorServlet has been initialized. ");
		System.out.println("******************************************");
			}
			public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
				PrintWriter	out = res.getWriter();
				
				out.println("<h1>You are now using the calculator app</h1><br>");
				out.println( "<div>To use the app, input two numbers and an operation.</div><br>");
				out.println("<div>Hit the submit button after filling in the details.</div><br>");
				out.println("<div>You will get the result shown in your browser.</div>");
			}
			public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
				System.out.println("Hello from the calculator servlet.");
				
				int num1 = Integer.parseInt(req.getParameter("num1"));
				int num2 = Integer.parseInt(req.getParameter("num2"));
				String operation = req.getParameter("operation");
				int result = 0;
				
				PrintWriter out = res.getWriter();
				
				if(operation.equals("add")) {
					result = num1 + num2;
				}if(operation.equals("subtract")) {
					result = num1 - num2;
				}if(operation.equals("multiply")) {
					result = num1 * num2;
				}if(operation.equals("divide")) {
					result = num1 / num2;
				}
				
				out.println("<div>The two numbers you provided are: " +num1+","+num2 + "</div><br>");
				out.println("<div>The operation that you wanted is: "+operation+ "</div><br>");
				out.println("<div>The result is: " +result+ "</div>");
			}
			public void destroy(){
				System.out.println("******************************************");
				System.out.println(" CalculatorServlet has been initialized. ");
				System.out.println("******************************************");
					}

}
